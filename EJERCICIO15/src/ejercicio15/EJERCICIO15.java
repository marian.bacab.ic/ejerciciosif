/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio15;

import java.util.Scanner;

/**
 *
 * @Frida Marian Bacab Ic
 */
public class EJERCICIO15 {

    /**
     * @param args the command line arguments
     */
     public static void main(String[] args) {
        Scanner consola=new Scanner(System.in);

        double cantidad, precio,pago, descuento;
        System.out.println("ingresar cantidad: ");
        cantidad=consola.nextInt();
        System.out.println("ingrese el precio: ");
        precio=consola.nextInt();

        pago=cantidad*precio;
        if(pago<50){
            descuento=pago*0.08;
            pago=pago-descuento;
            System.out.println("el descuento es: "+descuento+" , el pago a realizar es: "+pago);
        }
        else{           
             descuento=pago*0.1;
             pago=pago-descuento;
             System.out.println("el descuento es: "+descuento+" , el pago a realizar es: "+pago);
        }
    }

}
    
