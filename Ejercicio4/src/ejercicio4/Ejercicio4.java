/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio4;

import java.util.Scanner;

/**
 *
 * FRIDA MARIAN BACAB IC
 */
public class Ejercicio4 {

    /**
     * @param args the command line arguments
     */
   public static void main(String[] args) {
Scanner teclado = new Scanner(System.in);

int numDia = 0;

while (numDia<1 || numDia>7) {
System.out.println("Ingrese el número del día de la semana que desea saber");
numDia = teclado.nextInt();

if (numDia<1 || numDia>7) {
System.err.println("El número ingresado está fuera de rango.");
}
}
System.out.print("El número ingresado corresponde al día ");
switch (numDia){
case 1:
System.out.println("Domingo.");
break;
case 2:
System.out.println("Lunes.");
break;
case 3:
System.out.println("Martes.");
break;
case 4:
System.out.println("Miércoles.");
break;
case 5:
System.out.println("Jueves.");
break;
case 6:
System.out.println("Viernes.");
break;
default:
System.out.println("Sábado.");
break;
}
}
}
